import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule} from '@angular/http';
import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { ProductService } from './product/product.service';
import { EditProductComponent } from './edit-product/edit-product.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    EditProductComponent,
    AddproductComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ReactiveFormsModule,
    FormsModule,
    
    RouterModule.forRoot(
      [
        { path: '', component: ProductComponent },
        {path: 'editProducts/:id',component: EditProductComponent},
        {path: 'addProducts',component: AddproductComponent}
      ])
  ],
  providers: [ProductService],
  bootstrap: [AppComponent]
})

export class AppModule { }
