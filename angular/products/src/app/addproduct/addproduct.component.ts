import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product/product.service';
import { FormControl, FormGroup, Validators,ValidatorFn } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
})
export class AddproductComponent implements OnInit {
  
  errorMessage: string;
  products:any =[];
  newProduct = new FormGroup ({
    productId: new FormControl('',Validators.compose([Validators.required, Validators.max(9999)])),
    productName: new FormControl('',Validators.compose([Validators.required, Validators.pattern('[A-Z][a-z A-Z]*')])),
    description: new FormControl('',Validators.required),
    price: new FormControl('',Validators.required)   
  });
  constructor(private productService: ProductService,public route:Router) { }

  ngOnInit() {
    // if(this.productService.getNew()==undefined)
    //   this.productService.getProducts().subscribe(
    //     (response) => {this.products=response.json();console.log(this.products);
    //       this.productService.setNew(this.products)},
    //     function(error) { console.log("Error happened" + error)},
    //     function() {}
    //   );
    // else{
    //   this.products=this.productService.getNew();
    // }
  }
  validateAdd1() {
    let flag1 = false;
    if(!this.newProduct.controls['productId'].hasError('required')
        && !this.newProduct.controls['productName'].hasError('required')
        && !this.newProduct.controls['description'].hasError('required')
        && !this.newProduct.controls['price'].hasError('required')
      )
    {
      flag1 = true;
    }
    return flag1;
  }

  validateAdd2() {
    let flag2 = false;
    if(!this.newProduct.controls['productId'].hasError('max'))
    {
      flag2 = true;
    }
    return flag2;
  }

  validateAdd3() {
    let flag3 = false;
    if(!this.newProduct.controls['productName'].hasError('pattern'))
    {
      flag3 = true;
    }
    return flag3;
  }
  

  add(){
    if(this.productService.validateId(this.newProduct.value.productId)){
      if(this.validateAdd1() && this.validateAdd2() && this.validateAdd3())
      {
        this.productService.appendNewData(this.newProduct.value);
        this.route.navigate(['/']);
      }
      else if(!this.validateAdd1()){
        this.errorMessage = "please fill all the fields before submitting"; 
      }
      else if(!this.validateAdd2() && this.validateAdd3()){
        this.errorMessage = "ID can have maximum 4 digits"; 
      }
      else if(!this.validateAdd3() && this.validateAdd2()){
        this.errorMessage = "Product Name should start with capital letter"; 
      }
      else if(!this.validateAdd3() && !this.validateAdd2()){
        this.errorMessage = "ID can have maximum 4 digits and Product Name should start with a capital letter"; 
      }
    }
    else{
      this.errorMessage="Product with this ID already Exists"
    }
  }

}
