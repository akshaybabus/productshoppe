import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators,ValidatorFn } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../product/product.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html'
})
export class EditProductComponent implements OnInit {

  editingProduct: any = {};
  editId: number;
  editForm: any;
  errorMessage:string;
  
  constructor(public route:ActivatedRoute, public router:Router, public productService: ProductService ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
        this.editId = parseInt(params.id);
    });

    this.editingProduct = this.productService.getEditingProduct(this.editId);
    console.log(this.editingProduct);

    this.editForm = new FormGroup ({
      id: new FormControl(this.editingProduct.productId),
      name: new FormControl(this.editingProduct.productName,Validators.compose([Validators.required, Validators.pattern('[A-Z][a-z A-Z]*')])),
      description: new FormControl(this.editingProduct.description,Validators.required),
      price: new FormControl(this.editingProduct.price,Validators.required)
    });
    
  }

  validateEdit1() {
    let flag1 = false;
    if(!this.editForm.controls['name'].hasError('required')
        && !this.editForm.controls['description'].hasError('required')
        && !this.editForm.controls['price'].hasError('required')
      )
    {
      flag1 = true;
    }
    return flag1;
  }

  validateEdit2() {
    let flag3 = false;
    if(!this.editForm.controls['name'].hasError('pattern'))
    {
      flag3 = true;
    }
    return flag3;
  }

  editValues() {
    if(this.validateEdit1() && this.validateEdit2())
    {
      this.productService.edit(this.editId,this.editForm.value);
      this.router.navigate(['/']);
    }
    else if(!this.validateEdit1()){
      this.errorMessage = "please fill all the fields before submitting"; 
    }
    else if(!this.validateEdit2()){
      this.errorMessage = "Product Name should start with a capital letter"; 
    }
  }

  

}
