import { Injectable } from '@angular/core';
import { Http,Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class ProductService {
    new:any=[];
    constructor(private http : Http){
        
    }
    
    setNew(prod){
        this.new=prod;
        console.log("setNew",this.new);
    }

    getNew(){
        return this.new;
    }
    
    getProducts(){
        return this.http.get("../../assets/products.json");
    }

    appendNewData(newData){
        console.log("Appending... ",newData);
        this.new.push(newData);
    }

    validateId(id){
        var ids=this.new.map((n)=>{return n.productId});
        var i=ids.indexOf(id);
        if(i>=0)
            return false;
        else
            return true;
    }

    edit(id,editedObject){
        for(let i=0; i<this.new.length; i++)
        {
            if(this.new[i].productId == id)
            {
                console.log("found");
                this.new[i].productId = editedObject.id;
                this.new[i].productName = editedObject.name;
                this.new[i].description = editedObject.description;
                this.new[i].price = editedObject.price;
            }
        }
        console.log(editedObject);
        console.log(this.new);
    }

    getEditingProduct(id)
    {
        for(let i=0; i<this.new.length; i++)
        {
            if(this.new[i].productId == id)
            {
                return this.new[i];
            }
        }
    }

}