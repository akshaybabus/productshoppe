import { Component, OnInit } from '@angular/core';
import { ProductService } from './product.service';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  
})
export class ProductComponent implements OnInit {
  title: string = "Product Application!";
  products:any =[];
  product:any =[];
  j:any;
  pages:any=[];
  noOfPages:any=0;
  constructor(private productService: ProductService) { this.j=1; }

  ngOnInit() {

      console.log("The products are ",this.productService.getNew());
      if(this.productService.getNew().length==0)
      this.productService.getProducts().subscribe(
        (response) => {this.products=response.json();console.log(this.products);
          this.noOfPages=this.products.length/5;
          this.generatePagination();
          this.productService.setNew(this.products)
          this.product=this.productService.getNew();
          console.log("The new new",this.productService.getNew())
          console.log(this.product)}
          ,
        function(error) { console.log("Error happened" + error)},
        function() {}
      );
    else{
      this.products=this.productService.getNew();
      this.noOfPages=this.products.length/5;
      this.generatePagination();
      console.log(this.products);
      
    }
  }
  generatePagination(){
    for(let i=0;i<this.noOfPages;i++)
      this.pages[i]=i+1;
  }
  delete(i){
    this.products.splice(i, 1);
     if(this.products.length%5==0){
      this.pages.splice((this.pages.length-1),1);
      this.j=this.pages.length;
    }
  }

  deleteAll(){
    
   
    if(confirm("Are you sure you want to delete all"))
    {
      console.log(this.products.length);
      this.products.splice(0,this.products.length);
      this.j=1;
    }
  }

  prev(){
    if(this.j>1)
    {
      this.j-=1;
    }
    console.log(this.j);
  }

  changePage(j){
    this.j=j;
    console.log(this.j)
  }
  
  next(){
    this.j+=1;
    console.log(this.j);
  }
  
}
