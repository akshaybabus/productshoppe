var total=0;
var cartItems=0;
function start(){
    
    $(document).ready(function(){
    cart=JSON.parse(localStorage.getItem("cart"));
    if(cart){
        // cartItems=cart.length;

        
        cart.forEach(function (n) {
            /* body... */
            curr=getBookById(n.id)
            let p=curr.price;
            let q=n.qty;
            console.log(cartItems)
            cartItems=cartItems+q;
            total=total+p*q;
            console.log(p+" "+q+" "+total)
            $(".tableCart").append(`<tr>
                            <td>`+curr.name+`</td>
                            <td id="`+n.id+`">`+n.qty+`</td>
                            <td id="`+n.id+`price">`+curr.price+`</td>
                        </tr>`);



        })
        $("#cart").html('<span class="glyphicon glyphicon-shopping-cart" style="position: relative;top: 3px;"></span>&nbsp; Cart ('+cartItems+')')
        $(".tableCart").append("<tr id='total'><td colspan=2>Total:</td><td>"+total+"</td></tr>");
    }
    else{
        cart=[];
    }
    bestSellers.forEach((x)=>{
        n=getBookById(x);
        $("#best").append(`<div class="div-book" ondragstart='drag(event,this)'>
                                        <img src="`+n.imgurl+`" class="book-image"><br/>
                                        <div class="book-data">
                                            <input name="price" type="hidden" value=`+n.price+`>
                                            <input name="id" type="hidden" value=`+n.id+`
                                            <a href="#">`+n.name+`</a><br>
                                            <span>By `+n.author+`</span><br>
                                            <span><s>Rs. `+n.price+`</s></span> <span class="new-price">Rs. `+n.oldPrice+`</span>
                                        </div>
                                    </div>`)
    })
    newArrivals.forEach((x)=>{
        n=getBookById(x);
        $("#new").append(`<div class="div-book" ondragstart='drag(event,this)'>
                                        <img src="`+n.imgurl+`" class="book-image"><br/>
                                        <div class="book-data">
                                            <input name="price" type="hidden" value=`+n.price+`>
                                            <input name="id" type="hidden" value=`+n.id+`
                                            <a href="#">`+n.name+`</a><br>
                                            <span>By `+n.author+`</span><br>
                                            <span><s>Rs. `+n.price+`</s></span> <span class="new-price">Rs. `+n.oldPrice+`</span>
                                        </div>
                                    </div>`)
    })
    $(".right").hide();
    $('#cart').click(function(){
        console.log("Hi!")
        $(".left").toggle();
        $(".right").toggle();
    })
    $("#pay").click(function(){
        
        var cartstr=JSON.stringify(cart);
        console.log(cartstr);
        if(!cart || cart.length==0)
            alert("Cart Is Empty!")
        else
            {
                console.log(cart)
                localStorage.setItem("cart",cartstr);
                var loggedin=sessionStorage.getItem("loggedIn");
                if(loggedin)
                    window.open("shoppingCart.html","_self");
                else {
                    alert("Please Log In First");
                    $("#myModal").modal('toggle');
                }
            }
    })

})}
$(document).ready(function () {
    /* body... */
    $("#loginForm").submit(function(e){
        var email=$("#inputEmail").val();
        var pas=$("#inputPassword").val();
        var u=getUser(email,pas);
        if(u){
            console.log("in")
            sessionStorage.setItem("loggedIn", 1)
            alert("Login Successfull")
            $("#myModal").modal('toggle');
        }
        else{
            alert("Incorrect Credentials or user doesnt exist");
        }
        e.preventDefault()
    })

})
function drag(ev,el){
    console.log("hi!")
    p=$(el).find("input[name='price']").val();
    id=$(el).find("input[name='id']").val();
    console.log(p+","+id)
    ev.dataTransfer.setData("text",[p,id]);
}
function allowDrop(ev){
    ev.preventDefault();
}
function drop(e){
    var data=e.dataTransfer.getData("text")
    data=data.split(",")
    var p=parseInt(data[0]);
    var id=parseInt(data[1]);
    total=total+p;
    var q;
    curr=getBookById(id);
    var currbook={};
    currbook.id=id;
    console.log(total);
    // console.log($(".drop").length);
    if($("#"+id).length==0){
        $(".tableCart").append(`<tr>
                        <td>`+curr.name+`</td>
                        <td id="`+id+`">1</td>
                        <td id="`+id+`price">`+curr.price+`</td>
                    </tr>`);
        q=1;
        
    }
    else{
        q=parseInt($("#"+id).text())+1;
        $("#"+id).text(q);
        p=q*curr.price;
        $("#"+id+"price").text(p);
    }
    // console.log(q);
    currbook.qty=q;
    let time=new Date();
    currbook.time=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"][time.getMonth()]+" "+time.getDate()+","+time.getFullYear();
    updateCart(currbook);
    
    
    cartItems++;
    
    $("#total").remove();
    $(".tableCart").append("<tr id='total'><td colspan=2>Total:</td><td>"+total+"</td></tr>");
    $("#cart").html('<span class="glyphicon glyphicon-shopping-cart" style="position: relative;top: 3px;"></span>&nbsp; Cart ('+cartItems+')')
    
}