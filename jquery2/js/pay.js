var total=0;
later=[];
$.urlParam=function(name)  
{  
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);  
    return results[1] || 0;  
}

function start(){
    $(document).ready(function(){
        var loggedin=sessionStorage.getItem("loggedIn");
        if(loggedin){
            $('.signin').hide();
        }
    // cart=JSON.parse(decodeURIComponent($.urlParam('cart')));
    cart=localStorage.getItem("cart");
    cart=JSON.parse(cart);
    console.log(typeof(cart));
    
    cart.forEach(function(x) {
        n=getBookById(x.id);
        let op=n.oldPrice;
        let p=n.price;
        let q=x.qty;
        total=total+op*q;
        $("#cart").append( `<div class="row cart-item" id=`+x.id+`>
                                <input type="hidden" class="unitPrice" value="`+op+`">
                                <input type="hidden" class="qty" value="`+q+`">
                                <div class="col-lg-2" style="text-align: right;">
                                    Item added on `+x.time+`<br><br>
                                    <button class="btn btn-xs btn-pale-yellow later">Save for later</button><br><br>
                                    <button class="btn btn-xs btn-pale-yellow delete">Delete</button>
                                </div>
                                <div class="col-lg-6">
                                    <a href="#">`+n.name+`</a> - `+n.author+`;<br>
                                    Paperback<br>
                                    Condition: New<br>
                                    In Stock<br>
                                    <span class="small-text"><img src="img/truck.jpg" class="img-gift">Eligible for FREE Super Saver Shipping</span><br><br><br>
                                    <input type="checkbox" ><span class="orange-text"> Add gift-wrap/note</span> <img src="img/gift.jpg" class="img-gift"> (<a href="#" class="small-text">Learn More</a>)
                                </div>
                                <div class="col-lg-2">
                                    <span class="price">Rs.`+n.oldPrice*q+`</span><br>
                                    You Save:<br>
                                    <span class="discount-price">Rs.`+(p-op)*q+` (`+parseInt((p-op)*100/p)+`%) </span>
                                </div>
                                <div class="col-lg-2">
                                    <input type="number" class="form-control qtyField" style="width: 70px;" value='`+x.qty+`'>
                                </div>
                            </div>`)
    
    }, this);
    // console.log(total)
    $("#total").text(total);
    $(".delete").click(function(){
        var id=$(this).parent().parent().attr("id");
        deleteFromCart(parseInt(id));
        let p=($("#"+id+" .unitPrice").val())
        let q=$("#"+id+" .qty").val()
        total=total-p*q;
        $("#total").text(total);
        $("#"+id).remove();
        if(cart.length==0){
            alert("Cart is Now Empty!");
            console.log(cart);
            var cartstr=JSON.stringify(cart);
            localStorage.setItem("cart",cartstr);
            window.open("index.html","_self")
        }
    })
    $(".later").click(function(){
        $(this).parent().find(".delete").trigger('click');
    })
    $(".qtyField").blur(function(){
        let newq=parseInt($(this).val());
        let id=parseInt($(this).parent().parent().attr("id"));
        var qty=parseInt($(this).val());
        updateCartQty(id,qty);
        if(qty==0){
            $("#"+id).find(".delete").trigger("click");
        }
        // console.log(cart)
    })
    $("#updateBtn").click(function(){
        $(".qtyFeild").trigger('blur');
        cart.forEach(function(n){
            if(n.qty==0)deleteFromCart(n.id);
        })
        // console.log(cart);
        if(cart.length!=0){
            if(cart[cart.length-1].qty==0)deleteFromCart(cart[cart.length-1].id);
        }
        if(cart.length==0){
            alert("Your cart is empty!")
            var cartstr=JSON.stringify(cart);
            
        }
        else{
            var cartstr=JSON.stringify(cart);
            localStorage.setItem("cart",cartstr);
            window.open("shoppingCart.html","_self")
        }
    })
})
}