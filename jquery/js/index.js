$(document).ready(function(){
    var total=0;
    var cartItems=0;
    bestSellers.forEach((x)=>{
        n=getBookById(x);
        $("#best").append(`<div class="div-book draggable">
                                        <img src="`+n.imgurl+`" class="book-image"><br/>
                                        <div class="book-data">
                                            <input name="price" type="hidden" value=`+n.price+`>
                                            <input name="id" type="hidden" value="`+n.id+`">
                                            <a href="#">`+n.name+`</a><br>
                                            <span>By `+n.author+`</span><br>
                                            <span><s>Rs. `+n.oldPrice+`</s></span> <span class="new-price">Rs. `+n.price+`</span>
                                        </div>
                                    </div>`)
    });
    $("#best").append(`<div class="book-showcase-control">
    <a class="btn disabled glyphicon glyphicon-triangle-left control-button"></a>
    <a class="btn glyphicon glyphicon-triangle-right control-button"></a>
    </div> `);
    newArrivals.forEach((x)=>{
        n=getBookById(x);
        $("#new").append(`<div class="div-book draggable">
                                        <img src="`+n.imgurl+`" class="book-image"><br/>
                                        <div class="book-data">
                                            <input name="price" type="hidden" value=`+n.price+`>
                                            <input name="id" type="hidden" value="`+n.id+`">
                                            <a href="#">`+n.name+`</a><br>
                                            <span>By `+n.author+`</span><br>
                                            <span><s>Rs. `+n.oldPrice+`</s></span> <span class="new-price">Rs. `+n.price+`</span>
                                        </div>
                                    </div>`)
    });
    $("#new").append(`<div class="book-showcase-control">
    <a class="btn disabled glyphicon glyphicon-triangle-left control-button"></a>
    <a class="btn glyphicon glyphicon-triangle-right control-button"></a>
    </div> `);
    $(".right").hide();
    $('#cart').click(function(){
        console.log("Hi!")
        $(".left").toggle();
        $(".right").toggle();
    })
    
    $(".draggable").draggable({
        helper:'clone'
    });
    $(".drop").droppable({
        drop:function(e,ui){
            // console.log(e);
            p=parseInt($(ui.draggable).find("input[name='price']").val());
            id=parseInt($(ui.draggable).find("input[name='id']").val());
            total=total+p;
            var q;
            curr=getBookById(id);
            var currbook={};
            currbook.id=id;
            console.log(total);
            // console.log($(".drop").length);
            if($("#"+id).length==0){
                $(".tableCart").append(`<tr>
                                <td>`+curr.name+`</td>
                                <td id="`+id+`">1</td>
                                <td id="`+id+`price">`+curr.price+`</td>
                            </tr>`);
                q=1;
                
            }
            else{
                q=parseInt($("#"+id).text())+1;
                $("#"+id).text(q);
                p=q*curr.price;
                $("#"+id+"price").text(p);
            }
            // console.log(q);
            currbook.qty=q;
            let time=new Date();
            currbook.time=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"][time.getMonth()]+" "+time.getDate()+","+time.getFullYear();
            updateCart(currbook);
            
            
            cartItems++;
            
            $("#total").remove();
            $(".tableCart").append("<tr id='total'><td colspan=2>Total:</td><td>"+total+"</td></tr>");
            $("#cart").html('<span class="glyphicon glyphicon-shopping-cart" style="position: relative;top: 3px;"></span>&nbsp; Cart ('+cartItems+')')

        }
    })
    $("#pay").click(function(){
        
        var cartstr=JSON.stringify(cart);
        console.log(cartstr);
        if(cart.length==0)
            alert("Cart Is Empty!")
        else
            window.open("shoppingCart.html?cart="+cartstr,"_self")
    })

})