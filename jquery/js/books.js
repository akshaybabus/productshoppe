var books=[{
    id:1,
    name:"I Have A Dream",
    author:'Rashmi Bansal',
    imgurl:"img/book1.jpg",
    price:100,
    oldPrice:50
},
{
    id:2,
    name:"Siary of A Wimpy Kid",
    author:'Jeff Kinny',
    imgurl:"img/book2.jpg",
    price:110,
    oldPrice:50
},
{
    id:3,
    name:"She Broke up, I",
    author:'Durjoy Dutta',
    imgurl:"img/book3.jpg",
    price:101,
    oldPrice:50
},
{
    id:4,
    name:"The Sence of an ending",
    author:'Julian Barnes',
    imgurl:"img/book4.jpg",
    price:110,
    oldPrice:50
},
{
    id:5,
    name:"The Litigators",
    author:'John Girsham',
    imgurl:"img/book5.jpg",
    price:100,
    oldPrice:50
},
{
    id:6,
    name:"bnm",
    author:'hello!',
    imgurl:"img/book1.jpg",
    price:110,
    oldPrice:50
},
{
    id:7,
    name:"fgh",
    author:'hello!',
    imgurl:"img/book2.jpg",
    price:100,
    oldPrice:50
}]

var bestSellers=[1,2,3,4]
var newArrivals=[5,6,7]

var cart=[]

var users=[{
    name:"admin",
    email:"admin@admin",
    pass:"admin"
}]

var ids=books.map(function(n){
    return n.id;
})

function getBookById(id){
    var i=ids.indexOf(id);
    if(i>=0)return books[i];
    else return i;
}

function deleteFromCart(id){
    console.log(cart)
    var ind=cart.map((x)=>{return x.id}).indexOf(id);
    for(var i=ind;i<cart.length-1;i++){
        cart[i]=cart[i+1];
    }
    cart[i]=undefined;
    cart.length--;
    console.log(cart)
}

function updateCartQty(id,q){
    var ind=cart.map((x)=>{return x.id}).indexOf(id);
    cart[ind].qty=q;
}

function updateCart(bk){
    if(bk.qty==1)cart.push(bk);
    else{ 
        let i=cart.map((x)=>{return x.id}).indexOf(bk.id);
        console.log(bk);
        cart[i]=bk;
    }
    console.log(cart);
}